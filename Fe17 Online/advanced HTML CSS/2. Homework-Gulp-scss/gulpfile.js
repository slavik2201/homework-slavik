import gulp from "gulp";
import gulpSass from "gulp-sass";
import nodeSass from "sass";
const sass = gulpSass(nodeSass);
import bs from "browser-sync";
import imagemin from "gulp-imagemin";
const browserSync = bs.create();
const { src, dest, task, watch, series } = gulp;

const imgMin = () => {
  return gulp
    .src("src/styles/img/*")
    .pipe(imagemin())
    .pipe(gulp.dest("dist/styles/img"));
};

const convertCss = () =>
  src("src/styles/**/*").pipe(sass()).pipe(dest("dist/styles"));

const startWatching = () => {
  browserSync.init({
    server: {
      baseDir: "./",
    },
  });
  watch("src/styles/**/*").on("all", series(convertCss, browserSync.reload));
};

gulp.task("img", imgMin);

task("dev", startWatching);
task("build", convertCss);
