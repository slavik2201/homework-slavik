import React, { useState } from "react";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
import Title from "./components/Title/Title";
import style from "./App.module.scss";

function App() {
  const [firstModalActive, setfirstModalActive] = useState(false);
  const [secondModalActive, setsecondModalActive] = useState(false);

  const firstModal = () => {
    setfirstModalActive((prev) => {
      if (firstModalActive === !setfirstModalActive) {
        return true;
      } else {
        return false;
      }
    });
  };

  const secondModal = () => {
    setsecondModalActive((prev) => {
      if (secondModalActive === !setsecondModalActive) {
        return true;
      } else {
        return false;
      }
    });
  };

  return (
    <div className={style.container}>
      <Title text={"!!!Hello React!!!"} />
      <div className={style.buttonGroup}>
        <Button
          onClick={firstModal}
          backgroundColor={"rgb(255, 0, 234)"}
          text={"Open first modal"}
        />

        <Button
          onClick={secondModal}
          color={"rgb(255, 0, 234)"}
          backgroundColor={" rgb(13, 255, 0)"}
          text={"Open second modal"}
        />
      </div>
      {firstModalActive && (
        <Modal
          header={"First modal"}
          closeButton={false} //Нема хрестикa...
          isOpen={firstModal}
          textmodal={"-------------- First modal ---------------"}
          actions={
            <div className={style.modalFooter}>
              <Button
                text={"Ok"}
                color={"rgb(255, 255, 255)"}
                backgroundColor={"rgb(75, 0, 0)"}
              />
              <Button
                text={"Cancel"}
                onClick={firstModal}
                color={"rgb(255, 255, 255)"}
                backgroundColor={"rgb(75, 0, 0)"}
              />
            </div>
          }
        />
      )}
      {secondModalActive && (
        <Modal
          header={"Second modal"}
          closeButton={true}
          isOpen={secondModal}
          textmodal={"-------------- Second modal ---------------"}
          actions={
            <div className={style.modalFooter}>
              <Button
                text={"React"}
                color={"rgb(255, 255, 255)"}
                backgroundColor={"rgb(139, 134, 35)"}
              />
              <Button
                text={"ОК"}
                color={"rgb(255, 255, 255)"}
                backgroundColor={"rgb(28, 131, 123)"}
              />
              <Button
                text={"Cancel"}
                onClick={secondModal}
                color={"rgb(255, 255, 255)"}
                backgroundColor={"rgb(118, 27, 89)"}
              />
            </div>
          }
        />
      )}
    </div>
  );
}

export default App;
