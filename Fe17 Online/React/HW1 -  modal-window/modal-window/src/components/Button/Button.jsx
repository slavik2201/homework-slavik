import React from "react";
import styles from "./Button.module.scss";

const Button = (props) => {
  return (
    <button
      className={styles.borderButton}
      onClick={props.onClick}
      style={{
        color: props.color,
        backgroundColor: props.backgroundColor
          ? props.backgroundColor
          : "black",
      }}
    >
      {props.text}
    </button>
  );
};

export default Button;
