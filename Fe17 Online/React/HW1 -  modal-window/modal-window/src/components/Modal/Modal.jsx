import React from "react";
import styles from "./Modal.module.scss";

const Modal = (props) => {
  return (
    <div onClick={props.isOpen} className={styles.modal}>
      <div
        onClick={(event) => event.stopPropagation()}
        className={styles.modalContent}
      >
        <div className={styles.modalHeader}>
          <h5 className={styles.modalTitle}>{props.header}</h5>
          {props.closeButton && <span onClick={props.isOpen}>&#10761;</span>}
        </div>
        <div className={styles.modalBody}>
          <p className={styles.bodyText}>{props.textmodal}</p>
        </div>
        {props.actions}
      </div>
    </div>
  );
};

export default Modal;
