// 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Наслідування властивостей від батьківського класу
// 2.Для чого потрібно викликати super() у конструкторі класу-нащадка?
// Для того щоб передаты  батьківський конструктор дочірньому конструктору.

class Employee {
  _name = null;
  _age = null;
  _salary = 0;

  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  get name() {
    return this._name;
  }

  set name(value) {
    this._name = value;
  }

  get age() {
    return this._age;
  }

  set age(value) {
    this._age = value;
  }

  get salary() {
    return this._salary;
  }

  set salary(value) {
    this._salary = value;
  }
}

class Programmer extends Employee {
  constructor(lang, ...args) {
    super(...args);
    this._lang = lang;
  }
  get salary() {
    return this._salary;
  }
  set salary(value) {
    this._salary = value * 3;
  }
}

const Slavik1 = new Programmer("JS", "Slavik1", 24, 100);
const Slavik2 = new Programmer("JS", "Slavik2", 25, 200);
const Slavik3 = new Programmer("JS", "Slavik3", 26, 300);
const Slavik4 = new Programmer("JS", "Slavik4", 27, 400);
const Slavik5 = new Programmer("JS", "Slavik5", 28, 500);
console.log(Slavik1);
console.log(Slavik2);
console.log(Slavik3);
console.log(Slavik4);
console.log(Slavik5);
