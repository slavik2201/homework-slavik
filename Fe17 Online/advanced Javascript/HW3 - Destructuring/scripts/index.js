// Завдання 1

const clients1 = [
  "SLAVIK",
  "Гилберт",
  "Сальваторе",
  "Пирс",
  "Соммерс",
  "Форбс",
  "Донован",
  "Беннет",
];

const clients2 = ["SLAVIK", "Пирс", "Зальцман", "Сальваторе", "Майклсон"];

// Вариант1

const clients3 = clients1.concat(clients2);
const unique = (arr) => {
  return Array.from(new Set(arr));
};

console.log(unique(clients3));

// Вариант2

// const clients3 = clients1.concat(clients2);
// function unique(arr) {
//   return Array.from(new Set(arr));
// }

// console.log(unique(clients3));

// Вариант 3

// const clients3 = clients1.concat(clients2);
// function unique(arr) {
//   let result = [];
//   for (let str of arr) {
//     if (!result.includes(str)) {
//       result.push(str);
//     }
//   }
//   return result;
// }

// console.log(unique(clients3));

//Завдання 2

const characters = [
  {
    name: "Елена",
    lastName: "Гилберт",
    age: 17,
    gender: "woman",
    status: "human",
  },
  {
    name: "Кэролайн",
    lastName: "Форбс",
    age: 17,
    gender: "woman",
    status: "human",
  },
  {
    name: "Аларик",
    lastName: "Зальцман",
    age: 31,
    gender: "man",
    status: "human",
  },
  {
    name: "Дэймон",
    lastName: "Сальваторе",
    age: 156,
    gender: "man",
    status: "vampire",
  },
  {
    name: "Ребекка",
    lastName: "Майклсон",
    age: 1089,
    gender: "woman",
    status: "vempire",
  },
  {
    name: "Клаус",
    lastName: "Майклсон",
    age: 1093,
    gender: "man",
    status: "vampire",
  },
];

const charactersShortInfo = characters.slice();
// console.log(charactersShortInfo);
for (const elem of charactersShortInfo) {
  delete elem.gender;
  delete elem.status;
  // console.log(elem);
}
console.log(charactersShortInfo);
