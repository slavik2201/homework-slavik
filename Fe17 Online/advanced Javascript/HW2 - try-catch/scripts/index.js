const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

function hasIndex(arr, index) {
  for (const item of arr) {
    if (item?.index === index) return true;
  }
  return false;
}

function setKeys(arr, index, key) {
  for (const item of arr) {
    if (item.index === index) item.keys.push(key);
  }
}

function validate(arr) {
  const result = [];
  try {
    const errors = [];

    for (const [index, item] of Object.entries(arr)) {
      if (!item?.author) {
        if (hasIndex(errors, index)) {
          setKeys(errors, index, "author");
        } else {
          errors.push({
            index: index,
            keys: ["author"],
          });
        }
      }
      if (!item?.name) {
        if (hasIndex(errors, index)) {
          setKeys(errors, index, "name");
        } else {
          errors.push({
            index: index,
            keys: ["name"],
          });
        }
      }
      if (!item?.price) {
        if (hasIndex(errors, index)) {
          setKeys(errors, index, "price");
        } else {
          errors.push({
            index: index,
            keys: ["price"],
          });
        }
      }
    }
    if (errors.length === 0) return arr;
    else {
      const indx = [];

      for (const item of errors) {
        indx.push(Number(item.index));
      }
      for (let i = 0; i < books.length; i++) {
        if (!indx.includes(i)) result.push(books[i]);
      }
    }

    if (errors.length > 0) {
      throw errors;
    }
  } catch (error) {
    for (const item of error) {
      console.error(
        `Object with index: ${item.index} has not this keys - ${item.keys}`
      );
    }
  } finally {
    return result;
  }
}

function getList() {
  const validatedList = validate(books);
  // console.log(validatedList);
  let result = "";

  for (const item of validatedList) {
    result += `
      <li>
        ${item.name}
        </li>
        <li>
        ${item.author}
        </li>
        <li>
        ${item.price}
      </li>
  `;
  }
  return result;
}

// getList();

function createList() {
  const div = document.createElement("div");
  const ul = document.createElement("ul");

  div.id = "root";

  const list = getList();

  ul.innerHTML = list;
  div.append(ul);
  document.body.prepend(div);
}

createList();
