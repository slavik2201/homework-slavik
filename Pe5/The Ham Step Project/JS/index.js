const ourServices = [
  {
    image: "./img/Layer 9 1.png",
    text: "Texе1",
  },
  {
    image: "./img/Layer 9 1.png",
    text: "Text2",
  },
  {
    image: "./img/Layer 9 1.png",
    text: "Text3",
  },
  {
    image: "./img/Layer 9 1.png",
    text: "Text4",
  },
  {
    image: "./img/Layer 9 1.png",
    text: "Text5",
  },
  {
    image: "./img/Layer 9 1.png",
    text: "Text6",
  },
];

function selectService(event, index) {
  const container = document.querySelector(".img-web");

  if (container) {
    const children = Array.from(container.children);

    const img = children.filter((v) => v.tagName === "IMG")[0];

    img.setAttribute("src", ourServices[index].image);

    const p = children.filter((v) => v.tagName === "P")[0];

    p.innerText = ourServices[index].text;
  }
}

const services = Array.from(
  document.querySelectorAll(".main-option-services-list li")
);

if (services.length > 0) {
  for (let i = 0; i < services.length; i++) {
    services[i].addEventListener("click", (event) => {
      selectService(event, i);
      const activeLi = document.querySelector(
        ".main-option-services-list li.active"
      );
      if (activeLi) {
        activeLi.classList.remove("active");
      }
      event.target.classList.add("active");
    });
  }
}

const images = [
  {
    src: "./img/GraphicDesign/graphic-design1.jpg",
    category: "graphic",
  },
  {
    src: "./img/GraphicDesign/graphic-design2.jpg",
    category: "graphic",
  },
  {
    src: "./img/GraphicDesign/graphic-design3.jpg",
    category: "graphic",
  },
  {
    src: "./img/WebDesign/web-design1.jpg",
    category: "design",
  },
  {
    src: "./img/WebDesign/web-design2.jpg",
    category: "design",
  },
  {
    src: "./img/WebDesign/web-design3.jpg",
    category: "design",
  },
  {
    src: "./img/LandingPages/landing-page1.jpg",
    category: "landing",
  },
  {
    src: "./img/LandingPages/landing-page2.jpg",
    category: "landing",
  },
  {
    src: "./img/LandingPages/landing-page3.jpg",
    category: "landing",
  },
  {
    src: "./img/Wordpress/wordpress1.jpg",
    category: "press",
  },
  {
    src: "./img/Wordpress/wordpress2.jpg",
    category: "press",
  },
  {
    src: "./img/Wordpress/wordpress3.jpg",
    category: "press",
  },
];

const moreImages = [
  {
    src: "./img/GraphicDesign/graphic-design4.jpg",
    category: "graphic",
  },
  {
    src: "./img/GraphicDesign/graphic-design5.jpg",
    category: "graphic",
  },
  {
    src: "./img/GraphicDesign/graphic-design6.jpg",
    category: "graphic",
  },
  {
    src: "./img/LandingPages/landing-page4.jpg",
    category: "landing",
  },
  {
    src: "./img/LandingPages/landing-page5.jpg",
    category: "landing",
  },
  {
    src: "./img/LandingPages/landing-page6.jpg",
    category: "landing",
  },
  {
    src: "./img/WebDesign/web-design4.jpg",
    category: "design",
  },
  {
    src: "./img/WebDesign/web-design5.jpg",
    category: "design",
  },
  {
    src: "./img/WebDesign/web-design6.jpg",
    category: "design",
  },
  {
    src: "./img/Wordpress/wordpress4.jpg",
    category: "press",
  },
  {
    src: "./img/Wordpress/wordpress5.jpg",
    category: "press",
  },
  {
    src: "./img/Wordpress/wordpress6.jpg",
    category: "press",
  },
];

let moreClicked = false;
let imageType = "all";

function getImagesArray(type) {
  let imagesArray = [];
  if (moreClicked) {
    imagesArray = [...images, ...moreImages]; // spread operator
  } else {
    imagesArray = images;
  }

  return type === "all"
    ? imagesArray
    : imagesArray.filter((v) => v.category === type);
}

const categories = Array.from(document.querySelectorAll(".main-work-list li"));
const imagesContainer = document.querySelector(".main-work-img");

if (categories.length > 0) {
  for (const category of categories) {
    category.addEventListener("click", (e) => {
      const type = e.target.getAttribute("data-category");
      const images = getImagesArray(type);

      imageType = type;
      imagesContainer.innerHTML = "";
      for (const image of images) {
        const img = document.createElement("img");
        img.classList.add("img-graphicDesign");
        img.setAttribute("src", image.src);

        imagesContainer.insertAdjacentElement("beforeend", img);
      }
    });
  }
}

const workButton = document.querySelector(".main-work-button");

workButton.addEventListener("click", (e) => {
  moreClicked = true;
  const imagesContainer = document.querySelector(".main-work-img");
  const images = getImagesArray(imageType);
  imagesContainer.innerHTML = "";

  for (let i = 0; i < images.length; i++) {
    const img = document.createElement("img");
    img.classList.add("img-graphicDesign");
    img.setAttribute("src", images[i].src);

    imagesContainer.insertAdjacentElement("beforeend", img);
  }

  workButton.classList.add("is-disabled");
});

const peoples = [
  {
    name: "Hasan Ali 1",
    position: "UX Designer 1",
    image: "./img/Image1.png",
    alt: "img1",
  },
  {
    name: "Hasan Ali 2",
    position: "UX Designer 2",
    image: "./img/Image2.png",
    alt: "img2",
  },
  {
    name: "Hasan Ali 3",
    position: "UX Designer 3",
    image: "./img/Image3.png",
    alt: "img3",
  },
  {
    name: "Hasan Ali 4",
    position: "UX Designer 4",
    image: "./img/Image4.png",
    alt: "img4",
  },
];

function moveSlide(index) {
  const activeSlide = document.querySelector(".slider__slide.is-active");
  const currentSlide = document.querySelector(`[data-slide-index="${index}"]`);
  const previewH = document.querySelector(".active-slide h2");
  const previewP = document.querySelector(".active-slide p");
  const previewIMG = document.querySelector(".active-slide img");

  previewH.innerText = peoples[index].name;
  previewP.innerText = peoples[index].position;
  previewIMG.setAttribute("src", peoples[index].image);
  previewIMG.setAttribute("alt", peoples[index].alt);

  if (activeSlide) activeSlide.classList.remove("is-active");
  if (currentSlide) currentSlide.classList.add("is-active");
}

let slideIndx = 2;

const sliderPrev = document.querySelector(".slider__prev");
const sliderNext = document.querySelector(".slider__next");
const slides = Array.from(document.querySelectorAll(".slider__slide"));

for (const slide of slides) {
  slide.addEventListener("click", (e) => {
    const index = slide.getAttribute("data-slide-index");

    moveSlide(index);
    slideIndx = index;
  });
}

sliderPrev.addEventListener("click", () => {
  slideIndx--;
  if (slideIndx < 0) slideIndx = slides.length - 1;

  moveSlide(slideIndx);
});

sliderNext.addEventListener("click", () => {
  slideIndx++;
  if (slideIndx >= slides.length) slideIndx = 0;

  moveSlide(slideIndx);
});
