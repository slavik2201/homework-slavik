let input = document.createElement("input");
input.placeholder = "Price";
document.body.append(input);

const errorPrice = document.createElement("span");
const list = document.createElement("ul");
list.classList.add("list");

const pointed = (e) => {
  e.target.classList.remove("price", "input-text");
  e.target.classList.add("focus");
  errorPrice.remove();
  e.target.value = "";
};

const removed = (e) => {
  e.target.classList.remove("focus");
  checkValue();
};

const checkValue = () => {
  if (
    input.value < 0 ||
    input.value === "" ||
    Number.isNaN(parseFloat(input.value))
  ) {
    input.classList.add("price");
    errorPrice.innerText = "Please enter correct price";
    input.after(errorPrice);
  } else {
    input.classList.add("input-text");
    input.before(list);
    const li = document.createElement("li");
    li.classList.add("valide-price");
    const deleteBtn = createDelButton();
    li.innerText = `Текущая цена : ${input.value}`;
    li.append(deleteBtn);
    list.append(li);
  }
};

const createDelButton = () => {
  const delBtn = document.createElement("button");
  delBtn.innerText = "X";
  delBtn.style =
    "border: 1px solid black; border-radius: 30px; background-color: rgb(255, 0, 0);";
  delBtn.addEventListener("click", (e) => {
    e.target.closest("li").remove();
    input.value = "";
  });
  return delBtn;
};

input.addEventListener("focus", pointed);
input.addEventListener("blur", removed);
