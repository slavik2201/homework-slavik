// Ответ на теоретический вопрос.
// 1. var - устаревший метод обявления переменной, но тоже рабочий.
// let - Можно менять его значения в дальнейшом процесе.
// const - Нельзя менять значение переменой.
// 2. Устаревший способ его можно переобявлять сколько нам надо но сейчас не используют но его можно встретить. И дело в области видимости.

let userName;
let age;
do {
  userName = prompt("Введите имя");
} while (userName === "" || !isNaN(userName));
do {
  age = +prompt("Введите возраст");
} while (age === "" || !Number(age));
if (age < 18) {
  alert("You are not allowed to visit this website.");
} else if (age >= 18 && age <= 22) {
  if (confirm("Are you sure you want to continue?")) {
    alert(`Welcome,${userName}`);
  } else {
    alert("You are not allowed to visit this website.");
  }
} else {
  alert(`Welcome,${userName}`);
}
