let firstNumber;
let secondNumber;
let mathSign;
do {
  firstNumber = +prompt("Введите первое число");
} while (firstNumber === " " || !Number(firstNumber));

do {
  secondNumber = +prompt("Введите второе число");
} while (secondNumber === " " || !Number(secondNumber));

mathSign = prompt("Введите одно из +, -, /, *. ");

function calculate(firstNumber, secondNumber, mathSign) {
  switch (mathSign) {
    case "+":
      return firstNumber + secondNumber;
    case "-":
      return firstNumber - secondNumber;
    case "*":
      return firstNumber * secondNumber;
    case "/":
      return firstNumber / secondNumber;
    default:
      return "wrong value";
  }
}

const result = calculate(firstNumber, secondNumber, mathSign);
console.log(result);
