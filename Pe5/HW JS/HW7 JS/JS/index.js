let arrList = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

function elemList(arrList, parent = document.body) {
  let newList = arrList.map((item) => `<li>${item}</li>`);
  const ul = document.createElement("ul");
  for (const elem of newList) {
    ul.insertAdjacentHTML("beforeend", elem);
    parent.appendChild(ul);
  }
}
elemList(arrList);
