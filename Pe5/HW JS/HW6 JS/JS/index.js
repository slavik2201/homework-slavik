function filterBy(array, type) {
  const resultArray = array.filter(function (elem) {
    return typeof elem !== type;
  });
  return resultArray;
}

console.log(filterBy(["hello", "world", 23, "23", null], "string"));
