let number1;
let number2;
let symb;
do {
  number1 = +prompt("Введите первое число");
  number2 = +prompt("Введите второе число");
  symb = prompt("Введите одну из операций +, -, *, /.");
} while (
  !(number1 && !isNaN(+number1) && typeof +number1 === "number") &&
  !(number2 && !isNaN(+number2) && typeof +number2 === "number") &&
  symb !== "+" &&
  symb !== "-" &&
  symb !== "*" &&
  symb !== "/"
);

let resultat = thinks(number1, number2, symb);
function thinks(a, b, c) {
  switch (c) {
    case "+":
      return a + b;
    case "-":
      return a - b;
    case "*":
      return a * b;
    case "/":
      return a / b;
  }
}
console.log(resultat);
