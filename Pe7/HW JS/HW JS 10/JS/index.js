const form = document.querySelector(".password-form");

const toggleShowingPassword = (e) => {
  const input = e.target.parentElement.querySelector(".field");
  if (e.target.classList.contains("fa-eye-slash")) {
    e.target.classList.remove("fa-eye-slash");
    e.target.classList.add("fa-eye");
    input.type = "text";
  } else {
    e.target.classList.remove("fa-eye");
    e.target.classList.add("fa-eye-slash");
    input.type = "password";
  }
};

const message = document.createElement("span");

const compareValue = () => {
  const inputPassword = document.querySelector(".password");
  const confirmPassword = document.querySelector(".confirm-password");

  if (!inputPassword.value && !confirmPassword.value) {
    return;
  }

  if (inputPassword.value === confirmPassword.value) {
    alert("You are welcome!!!");
    message.remove();
  } else {
    message.textContent = "Нужно ввести одинаковые значения !!!";
    message.classList.add("color-span-error");
    confirmPassword.after(message);
  }
};

form.addEventListener("click", (e) => {
  e.preventDefault();

  if (e.target.classList.contains("icon-password")) {
    toggleShowingPassword(e);
    return;
  }

  if (e.target.classList.contains("btn")) {
    compareValue();
    return;
  }
});
