let array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
//let array = ["1", "2", "3", "sea", "user", 23];

function names(array, parent = document.body) {
  let newArray = array.map((e) => `<li>${e}</li>`);

  const ul = document.createElement("ul");

  for (const element of newArray) {
    ul.insertAdjacentHTML("beforeend", element);
  }
  parent.appendChild(ul);
}
names(array);
