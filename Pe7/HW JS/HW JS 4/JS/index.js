function createNewUser() {
  const userName = prompt("Enter your name");
  const userLastName = prompt("Enter last name");

  const newUser = {
    firstName: `${userName}`,
    lastName: `${userLastName}`,
    getLogin() {
      return (`${userName}`.charAt(0) + `${userLastName}`).toLowerCase();
    },
  };
  return newUser;
}

let newUser = createNewUser();
console.log(newUser.getLogin());
// console.log(createNewUser());
