const btnspot = document.querySelector("#stop");
const btnstart = document.querySelector("#start");
const img = document.querySelectorAll(".image");

let iterator = 0;
let timeout = null;
let interval = null;

btnstart.addEventListener("click", resume);
btnspot.addEventListener("click", stop);

function resume() {
  interval = setInterval(() => {
    if (iterator <= img.length) {
      setTimeout(() => {
        if (img[iterator].classList.contains("active")) {
          img[iterator].classList.remove("active");
        } else img[iterator].classList.add("active");
        iterator++;
      });
    } else if ([iterator] === img.length) {
      img[iterator].classList.add("active");
      iterator = 0;
    }
  }, 3000);
}

function stop() {
  clearInterval(interval);
}
