// function createNewUser() {
//   const userName = prompt("Enter your name");
//   const userLastName = prompt("Enter last name");
//   const age = prompt("Enter your age", "mm.dd.yyyy");

//   const newUser = {
//     firstName: `${userName}`,
//     lastName: `${userLastName}`,
//     birthday: `${age}`,

//     getLogin() {
//       //return (`${userName}`.charAt(0) + `${userLastName}`).toLocaleLowerCase();
//       return (this.firstName.charAt(0) + this.lastName).toLocaleLowerCase();
//     },

//     getAge() {
//       const myDate = new Date(this.birthday);
//       const today = new Date();
//       let age = today.getFullYear() - myDate.getFullYear();
//       return String(age);
//     },

//     getPassword() {
//       return (
//         this.firstName.charAt(0).toLocaleUpperCase() +
//         this.lastName.toLocaleLowerCase() +
//         age.substring(age.length - 4)
//       );
//     },
//   };

//   return newUser;
// }
// let newUser = createNewUser();

// console.log(newUser.getAge());
// console.log(newUser.getPassword());
// console.log(new Date("01.22.1999").toString);

function createNewUser() {
  const userName = prompt("Enter your name");
  const userLastName = prompt("Enter last name");
  const age = prompt("Enter your age", "mm.dd.yyyy");

  const newUser = {
    firstName: `${userName}`,
    lastName: `${userLastName}`,
    birthday: `${age}`,

    getLogin() {
      //return (`${userName}`.charAt(0) + `${userLastName}`).toLocaleLowerCase();
      return (this.firstName.charAt(0) + this.lastName).toLocaleLowerCase();
    },

    getAge() {
      const myDate = new Date(this.birthday);
      const today = new Date();
      let age = today.getFullYear() - myDate.getFullYear();
      return String(age);
    },

    getPassword() {
      return (
        this.firstName.charAt(0).toLocaleUpperCase() +
        this.lastName.toLocaleLowerCase() +
        age.substring(age.length - 4)
      );
    },
  };

  return newUser;
}
let newUser = createNewUser();

console.log(newUser.getAge());
console.log(newUser.getPassword());
