document.addEventListener("keypress", key);

function key(e) {
  const btn = document.querySelectorAll(".btn");
  btn.forEach((elem) => {
    if (e.key) {
      elem.classList.remove("color");
    }
    if (elem.innerText.toLowerCase() == e.key.toLowerCase()) {
      elem.classList.add("color");
    }
  });
}
