const tabs = document.querySelector(".tabs");
const tabsContent = document.querySelectorAll(".item");

tabs.addEventListener("click", (event) => {
  const clickLi = Array.from(tabs.children);
  //const clickLi = tabs.querySelectorAll(".tabs-title");
  clickLi.forEach((eachoneTitle) => {
    eachoneTitle.classList.remove("active");
  });
  event.target.classList.add("active");
  let newTabAttr = event.target.getAttribute("data-item");
  showDescription(newTabAttr);
});

const showDescription = (attr) => {
  tabsContent.forEach((tabContent) => {
    if (tabContent.getAttribute("data-item") === attr) {
      tabContent.classList.remove("hiding");
    } else {
      tabContent.classList.add("hiding");
    }
  });
};
