document.body.innerText = "Price:";

let inpu = document.createElement("input");
inpu.setAttribute("type", "number");
document.body.append(inpu);

inpu.addEventListener("mouseover", over);
inpu.addEventListener("mouseout", eout);

function over() {
  inpu.style = " outline: none; border: 5px solid lime; border-radius: 10px";
}
function eout() {
  inpu.style = "none; outline: none;";
  if (inpu.value > 0) {
    let spann = document.createElement("span");
    spann.style = "display: block; margin: 10px";
    spann.innerText = `Текущая цена : ${inpu.value}`;
    document.body.prepend(spann);
    inpu.style = "color: lime";

    const btn = document.createElement("button");
    btn.innerText = "X";
    btn.style = "margin-left: 10px";
    spann.append(btn);
    btn.addEventListener("click", delit);
    function delit(span) {
      span.target.closest("span").remove();
      inpu.value = "";
    }
  } else if (inpu.value < 0) {
    let spannn = document.createElement("span");
    spannn.style = "display: block; margin: 10px";
    inpu.style = " outline: none; border: 5px solid red; border-radius: 10px";
    spannn.innerText = "Please enter correct price";
    document.body.append(spannn);
    const okbtn = document.createElement("button");
    okbtn.innerText = "OK";
    okbtn.style = "margin-left: 10px";
    spannn.append(okbtn);
    okbtn.addEventListener("click", correct);
    function correct(spannn) {
      spannn.target.closest("span").remove();
      inpu.value = "";
    }
  }
}
